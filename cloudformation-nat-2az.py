cft = CloudFormationTemplate(description="HCDO 2 AZ NAT instance Stack")

cft.parameters.add(
    Parameter('TeamName', 'String',
        {
            'Description': 'The name of the application team in which the resources in this AWS account are named after',
        }
    )
)

cft.parameters.add(
    Parameter('Environment', 'String',
        {
            'Default': 'dev',
            'Description': 'Name of the environment',
            'AllowedValues': ['dev','prod'],
        }
    )
)

cft.parameters.add(
    Parameter('AccountType', 'String',
        {
            'Default': 'standard',
            'Description': 'Account Type, one of: standard, level4, bcdr',
            'AllowedValues': ['standard','level4','bcdr'],
            'ConstraintDescription': 'Must match standard or level4 or bcdr',
        }
    )
)

cft.parameters.add(
    Parameter('Product', 'String',
        {
            'Description': 'Product name as listed in ServiceNow',
            'Default': 'NAT'
        }
    )
)

cft.parameters.add(
    Parameter('HUITAssetId', 'String',
        {
            'Description': 'Asset ID as listed in Slash',
            'Default': '-1'
        }
    )
)

cft.parameters.add(
    Parameter('VpcCidr', 'String',
        {
            'Description': 'VPC CIDR block',
        }
    )
)

cft.parameters.add(
    Parameter('VpcId', 'String',
        {
            'Description': 'The physical ID of the VPC',
        }
    )
)

cft.parameters.add(
    Parameter('NatRegionImageId', 'String',
        {
            'Default': 'ami-184dc970',
            'Description': 'NAT AMI image ID',
        }
    )
)

cft.parameters.add(
    Parameter('NatInstanceType', 'String',
        {
            'Default': 't2.medium',
            'Description': 'NAT instance type',
        }
    )
)

cft.parameters.add(
    Parameter('NatPublicSubnet0', 'String',
        {
            'Description': 'NAT public subnet ID 0',
        }
    )
)

cft.parameters.add(
    Parameter('NatPublicSubnet1', 'String',
        {
            'Description': 'NAT public subnet ID 1',
        }
    )
)

cft.parameters.add(
    Parameter('PrivateRoute0', 'String',
        {
            'Description': 'Private route table ID 0',
        }
    )
)

cft.parameters.add(
    Parameter('PrivateRoute1', 'String',
        {
            'Description': 'Private route table ID 1',
        }
    )
)

cft.mappings.add(
  Mapping('EnvironmentMap',
    {
      'EnvironmentTranslation': {
        'dev': 'Development',
        'test': 'Test',
        'stage': 'Stage',
        'prod': 'Production'
      }
    }
  )
)

cft.resources.add(
    Resource('NatInstanceSecurityGroup', 'AWS::EC2::SecurityGroup',
        {
            'GroupDescription': 'Allows external communication from private subnets',
            'VpcId': ref('VpcId'),
            'SecurityGroupIngress': [
               {
                   'IpProtocol': 'tcp',
                   'FromPort': 0,
                   'ToPort': 65535,
                   'CidrIp': ref('VpcCidr')
               }
            ],
            'Tags': [
                {'Key': 'Name', 'Value': join('-', ref('TeamName'), ref('Environment'), ref('AccountType'), 'nat-sg')},
                {'Key': 'environment', 'Value': find_in_map('EnvironmentMap', 'EnvironmentTranslation', ref('Environment'))},
                {'Key': 'product', 'Value': ref('Product')},
                {'Key': 'huit_assetid', 'Value': ref('HUITAssetId')},
            ],
        }
    )
)

cft.resources.add(
    Resource('NatResourceInstance0', 'AWS::EC2::Instance',
         {
            'ImageId': ref('NatRegionImageId'),
            'InstanceType': ref('NatInstanceType'),
            'SourceDestCheck': 'false',
            'Tags': [
                {'Key': 'Name', 'Value': join('-', ref('TeamName'), ref('Environment'), ref('AccountType'), 'nat0')},
                {'Key': 'environment', 'Value': find_in_map('EnvironmentMap', 'EnvironmentTranslation', ref('Environment'))},
                {'Key': 'product', 'Value': ref('Product')},
                {'Key': 'huit_assetid', 'Value': ref('HUITAssetId')},
            ],
            'NetworkInterfaces': [
                {
                    'GroupSet': [ ref('NatInstanceSecurityGroup') ],
                    'AssociatePublicIpAddress': 'true',
                    'DeviceIndex': '0',
                    'DeleteOnTermination': 'true',
                    'SubnetId': ref('NatPublicSubnet0'),
                }
            ]
         }
    )
)

cft.resources.add(
    Resource('NatResourceInstance1', 'AWS::EC2::Instance',
         {
            'ImageId': ref('NatRegionImageId'),
            'InstanceType': ref('NatInstanceType'),
            'SourceDestCheck': 'false',
            'Tags': [
                {'Key': 'Name', 'Value': join('-', ref('TeamName'), ref('Environment'), ref('AccountType'), 'nat1')},
                {'Key': 'environment', 'Value': find_in_map('EnvironmentMap', 'EnvironmentTranslation', ref('Environment'))},
                {'Key': 'product', 'Value': ref('Product')},
                {'Key': 'huit_assetid', 'Value': ref('HUITAssetId')},
            ],
            'NetworkInterfaces': [
                {
                    'GroupSet': [ ref('NatInstanceSecurityGroup') ],
                    'AssociatePublicIpAddress': 'true',
                    'DeviceIndex': '0',
                    'DeleteOnTermination': 'true',
                    'SubnetId': ref('NatPublicSubnet1'),
                }
            ]
         }
    )
)

cft.resources.add(
    Resource('NatRouteTableEntry0', 'AWS::EC2::Route',
        {
            'RouteTableId': ref('PrivateRoute0'),
            'DestinationCidrBlock': '0.0.0.0/0',
            'InstanceId': ref('NatResourceInstance0'),
        }
    )
)

cft.resources.add(
    Resource('NatRouteTableEntry1', 'AWS::EC2::Route',
        {
            'RouteTableId': ref('PrivateRoute1'),
            'DestinationCidrBlock': '0.0.0.0/0',
            'InstanceId': ref('NatResourceInstance1'),
        }
    )
)

cft.outputs.add(
    Output('NatResourceInstanceId0',
           ref('NatResourceInstance0'), 'NAT Resource Instance ID 0'
    )
)

cft.outputs.add(
    Output('NatResourceInstancePublicIp0',
           get_att('NatResourceInstance0', 'PublicIp'), 'NAT Resource Instance 0 public IP'
    )
)

cft.outputs.add(
    Output('NatResourceInstanceId1',
           ref('NatResourceInstance1'), 'NAT Resource Instance ID 1'
    )
)

cft.outputs.add(
    Output('NatResourceInstancePublicIp1',
           get_att('NatResourceInstance1', 'PublicIp'), 'NAT Resource Instance 1 public IP'
    )
)

cft.outputs.add(
    Output('NatInstanceSecurityGroupId',
           ref('NatInstanceSecurityGroup'), 'NAT Resource Instance Security Group ID'
    )
)
