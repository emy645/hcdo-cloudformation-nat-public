## HCDO CloudFormation for NAT instances

* Parameterized
    * TeamName
    * Environment
    * AccountType
    * Product
    * HUITAssetId
    * VpcCidr
    * VpcId
    * NatRegionImageId
    * NatInstanceType
    * NatPublic0
    * NatPublic1
    * NatPublic2 (on 3 AZ template)
    * PublicRoute0
    * PublicRoute1
    * PublicRoute2 (on 3 AZ template)
* Creates route table entries
* Supports 2 or 3 AZ's
